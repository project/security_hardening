<?php

/**
 * @file
 * Tests for security hardening module.
 */

/**
 * Base class for functional tests.
 */
class SecurityHardeningTestCase extends DrupalWebTestCase {

  /**
   * Implements getInfo().
   */
  public static function getInfo() {
    return array(
      'name' => 'Security hardening',
      'description' => 'Tests security hardening module features.',
      'group' => 'security_hardening',
    );
  }

  /**
   * Implements setUp().
   */
  public function setUp() {
    parent::setUp(array('security_hardening'));
  }

  /**
   * Test that head meta tag generator is removed.
   */
  public function testNoMetaTagGenerator() {
    $this->drupalGet('<front>');
    $this->assertResponse(200);
    $this->assertNoRaw('<meta name="Generator" content="Drupal 7');
  }

  /**
   * Tests message in password recorvery form.
   */
  public function testPasswordRememberMessage() {
    $account = $this->drupalCreateUser();

    $this->drupalGet('user/password');
    $this->assertResponse(200);

    $message = t('Further instructions have been sent to your email address if it exists. If you do not receive anything, validate that the user or email you entered are correct.');

    // Valid username.
    $this->drupalPost('user/password', array('name' => $account->name), t('E-mail new password'));
    $this->assertText($message);

    // Invalid username.
    $this->drupalPost('user/password', array('name' => 'usernamefail'), t('E-mail new password'));
    $this->assertText($message);

    // Valid email.
    $this->drupalPost('user/password', array('name' => $account->mail), t('E-mail new password'));
    $this->assertText($message);

    // Invalid email.
    $this->drupalPost('user/password', array('name' => 'mailfail@example.com'), t('E-mail new password'));
    $this->assertText($message);
  }

}
