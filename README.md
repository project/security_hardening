INTRODUCTION
------------

This module overwrites some Drupal features to security hardening.

 * Remove head meta tag generator to reduce fingerprint software information.

 * Adds XSS protection header in block mode.

 * Adds Strict-Transport-Security header to tell browsers that the site it
   should only be accessed using HTTPS.

 * Prevents user enumeration using password request form. Drupal core shows
   different messages when you input a valid or invalid email o username, this
   module equals messages in both situations to avoid user enumeration.


REQUIREMENTS
------------

This module requires Drupal 7.x core.


INSTALLATION
------------

* If you use composer adds using: composer require 'drupal/security_hardening'

* If not download the module and deploy in modules/contrib folder.

* Enable the module in Admin menu > Site building > Modules.


MAINTAINERS
-----------

Current maintainers:

 * Oriol Roselló Castells (oriol_e9g) - https://www.drupal.org/u/oriol_e9g

For additional information, see the project page on:
https://www.drupal.org/project/security_hardening

To submit bug reports and feature suggestions, or to track changes visit:
https://www.drupal.org/project/issues/security_hardening


SPONSOR
-------

This module is sponsored by Diputació de Barcelona: https://www.diba.cat
